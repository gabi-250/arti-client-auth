# Testing client auth in Arti

Here we describe how to use Arti to connect to a hidden service that requires
client authorization. Note that the instructions here should all be run on the
same machine. They will not work if the client and hidden service are running on
different machines (the script that generates the client auth keys for Arti
also generates a corresponding entry in the `client_authorization` directory of
the hidden service we're testing against).

 1. Set up a hidden service. For example, you can run one locally by running
   `tor -f torrc`, where `torrc` is a file with the following contents:

 ```
 HiddenServiceDir HIDDEN-SVC-DIR
 HiddenServiceVersion 3
 HiddenServicePort 80 <some locally running service>
 ```

 2. Generate the necessary client auth keys using the `keymgr-cli` from this
    repository:

  ```
  cargo run --bin keymgr-cli -- \
                --arti-keystore PATH-TO-KEYSTORE \
                --client-onion-auth-dir HIDDEN-SVC-DIR \
                --client-name default \
                --hsid $(cat HIDDEN-SVC-DIR/hostname)
  ```

  This will create a private key for the Arti hidden service client at
  `<PATH-TO-KEYSTORE>/default/<HIDDEN-SVC-HOSTNAME>.onion/KP_hsc_desc_enc.x25519_private`,
  and an `authorized_clients` entry for the C Tor hidden service at
  `<HIDDEN-SVC-DIR>/authorized_clients/default.auth`.
  It will also create the necessary directories, as well as any missing parent
  directories in the two paths.

 3. Restart the hidden service

 4. Run Arti client using `cargo run --features keymgr,onion-service-client
    --bin arti proxy -c config.toml`, where `config.toml` is a file with the
    following contents:

  ```toml
 [address_filter]
 allow_onion_addrs = true

 [storage]
 keystore_dir = "PATH-TO-KEYSTORE"
  ```

 5. Use Arti to connect to the hidden service: `curl -x socks5h://127.0.0.1:9150 http://$(cat HIDDEN-SVC-DIR/hostname)`
