mod args;

use args::Args;

use clap::Parser;
use std::fs;
#[cfg(target_family = "unix")]
use std::os::unix::fs::PermissionsExt;

use rand_core::OsRng;
use tor_keymgr::EncodableKey;
use tor_llcrypto::pk::curve25519;
use tor_llcrypto::util::rand_compat::RngCompatExt;

const DIR_PERMS: u32 = 0o700;
const KEY_PERMS: u32 = 0o600;
const HS_CLIENT_DESC_ENC_KEY: &str = "KP_hsc_desc_enc.x25519_private";

fn write_arti_client_secret_key(x25519_sk: &curve25519::StaticSecret, args: &Args) {
    let ssh_keypair_data = x25519_sk.as_ssh_keypair_data().unwrap();
    let openssh_key = ssh_key::private::PrivateKey::new(ssh_keypair_data, "test-key").unwrap();

    fs::create_dir_all(&args.arti_keystore).unwrap();

    #[cfg(target_family = "unix")]
    fs::set_permissions(&args.arti_keystore, fs::Permissions::from_mode(DIR_PERMS)).unwrap();

    let path_components = &["client", &args.client_name, &args.hsid];
    let mut key_dir = args.arti_keystore.clone();

    for comp in path_components {
        key_dir = key_dir.join(comp);
        fs::create_dir_all(&key_dir).unwrap();

        #[cfg(target_family = "unix")]
        fs::set_permissions(&key_dir, fs::Permissions::from_mode(DIR_PERMS)).unwrap();
    }

    let openssh_key = openssh_key
        .to_openssh(ssh_key::LineEnding::LF)
        .unwrap()
        .to_string();
    fs::write(key_dir.join(HS_CLIENT_DESC_ENC_KEY), openssh_key).unwrap();

    #[cfg(target_family = "unix")]
    fs::set_permissions(
        key_dir.join(HS_CLIENT_DESC_ENC_KEY),
        fs::Permissions::from_mode(KEY_PERMS),
    )
    .unwrap();
}

fn write_ctor_authorized_client(x25519_pk: &curve25519::PublicKey, args: &Args) {
    // Prepare the authorized_clients entry for the HS service
    let x25519_pk = base32::encode(
        base32::Alphabet::RFC4648 { padding: false },
        &x25519_pk.to_bytes(),
    );

    let key_dir = args.client_onion_auth_dir.join("authorized_clients");
    fs::create_dir_all(&key_dir).unwrap();

    #[cfg(target_family = "unix")]
    fs::set_permissions(
        &args.client_onion_auth_dir,
        fs::Permissions::from_mode(DIR_PERMS),
    )
    .unwrap();

    let ctor_authorized_client = format!("descriptor:x25519:{}", x25519_pk);
    fs::write(
        key_dir.join(format!("{}.auth", args.client_name)),
        ctor_authorized_client,
    )
    .unwrap();
}

fn main() {
    let args = Args::parse();
    let x25519_sk = curve25519::StaticSecret::new(OsRng.rng_compat());
    let x25519_pk = curve25519::PublicKey::from(&x25519_sk);
    write_arti_client_secret_key(&x25519_sk, &args);
    write_ctor_authorized_client(&x25519_pk, &args);
}
