use clap::Parser;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub(crate) struct Args {
    /// The keystore directory
    #[arg(long)]
    pub(crate) arti_keystore: PathBuf,

    /// The `ClientOnionAuthDir` of your hidden service
    #[arg(long)]
    pub(crate) client_onion_auth_dir: PathBuf,

    /// The client name
    #[arg(long)]
    pub(crate) client_name: String,

    /// The hsid
    #[arg(long)]
    pub(crate) hsid: String,
}
